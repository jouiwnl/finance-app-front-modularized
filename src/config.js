import axios from 'axios';

export const http = axios.create({
    baseURL: 'http://localhost:8080/'
    //baseURL: 'https://teste-app-finance.herokuapp.com/'
    //baseURL: 'http://34.95.144.201/'
});