const Module = () => import("./Module.vue");
const Home = () => import("./views/Home.vue");
const Loan = () => import("./views/Loan.vue");
const LoanReport = () => import("./views/LoanReport.vue");

const moduleRoute = {
  path: "/loans",
  component: Module,
  children: [
    {
      path: "/",
      component: Home,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: ":id",
      component: Loan,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: ":id/report",
      component: LoanReport,
      meta: {
        requiresAuth: true
      }
    }

  ]
};

export default router => {
  router.addRoutes([moduleRoute]);
};
