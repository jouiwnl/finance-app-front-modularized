const Module = () => import("./Module.vue");
const Home = () => import("./views/Home.vue");
const Partner = () => import("./views/Partner.vue");

const moduleRoute = {
  path: "/partners",
  component: Module,
  children: [
    {
      path: "/",
      component: Home,
      meta: {
        requiresAuth: true
      }
    },

    {
      path: ":id",
      component: Partner,
      meta: {
        requiresAuth: true
      }
    }
  ]
};

export default router => {
  router.addRoutes([moduleRoute]);
};
